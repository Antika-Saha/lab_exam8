<?php

namespace App\City;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class City extends DB
{

    public $id = "";

    public $name = "users";

    public $city_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('city_name',$postVariableData)){
            $this->city_name=$postVariableData['city_name'];
        }

    }

    public function store(){


        $arrData = array($this->name,$this->city_name);
        $sql = "Insert INTO city(name,city_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store
    public function index($fetchMode='ASSOC'){

        $sql="SELECT * from city where is_deleted = 0";

        $STH = $this->DBH->query($sql);//needed

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);//needed
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrAllData  = $STH->fetchAll();//needed
        return $arrAllData;//needed


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from city where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetch();
        return $arrAllData;


    }// end of index();

    public function update(){
        $arrData=array($this->name,$this->city_name);
        $sql="UPDATE city SET name = ? , city_name = ? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }//end of update


    public function delete(){

        $sql = "Delete from city where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update city SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

}// end of BookTitle class