<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;
class BookTitle extends DB
{

    public $id = "";

    public $book_title = "users";

    public $author_name = "";


    public function __construct()
    {
        parent::__construct();
    }




    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('book_title',$postVariableData)){
            $this->book_title=$postVariableData['book_title'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name=$postVariableData['author_name'];
        }

    }

public function store(){

    $arrData = array($this->book_title,$this->author_name);
    $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";

    $STH = $this->DBH->prepare($sql);

   $result= $STH->execute($arrData);

    if($result)
        Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
    else
    Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

    Utility::redirect('creat.php');

}//end of store


    public function index($fetchMode='ASSOC'){

        $sql="SELECT * from book_title where is_deleted = 0";

        $STH = $this->DBH->query($sql);//needed

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);//needed
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);


        $arrAllData  = $STH->fetchAll();//needed
        return $arrAllData;//needed


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetch();
        return $arrAllData;


    }// end of index();

    public function update(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="UPDATE book_title SET book_title = ? , author_name = ? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }//end of update

    public function delete(){

        $sql = "Delete from book_title where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()

    public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



}// end of BookTitle class