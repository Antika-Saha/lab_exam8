<?php

namespace App\Hobbies;
use   App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB
{

    public $id = "";

    public $name = "users";

    public $hobbies = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($postVariableData=NULL){
        if(array_key_exists('id',$postVariableData)){
            $this->id=$postVariableData['id'];
        }

        if(array_key_exists('name',$postVariableData)){
            $this->name=$postVariableData['name'];
        }

        if(array_key_exists('hobbies',$postVariableData)){
            $this->hobbies=implode(",",$postVariableData['hobbies']);
        }

    }

    public function store(){


        $arrData = array($this->name,$this->hobbies);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);


        if($result)
            Message::setMessage("success!!!! Data has been inserted successfully.... :) ");
        else
            Message::setMessage("Faild!! Data has not been inserted successfully.... :( ");

        Utility::redirect('creat.php');

    }//end of store


    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from hobbies where is_deleted = 0";

        $STH = $this->DBH->query($sql);//needed

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);//needed
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();//needed
        return $arrAllData;//needed


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from hobbies where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetch();
        return $arrAllData;


    }// end of index();

    public function update(){
        $arrData=array($this->name,$this->hobbies);
        $sql="UPDATE hobbies SET name = ? , hobbies = ? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }//end of update

    public function delete(){

        $sql = "Delete from hobbies where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update hobbies SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


}// end of BookTitle class